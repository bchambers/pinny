let openReq = indexedDB.open('db', 2);
let db, pin, msgTimeout;

openReq.onupgradeneeded = function(event) {
    db = openReq.result;

    if(event.oldVersion < 1) {
        db.createObjectStore('pins',   { keyPath: 'id', autoIncrement: true });
        db.createObjectStore('places', { keyPath: 'id', autoIncrement: true });
    }

    if(event.oldVersion < 2) {
        let pins = openReq.transaction.objectStore('pins');
        pins.createIndex('timestamp', 'timestamp');
    }
}

openReq.onsuccess = function() {
    db = openReq.result;
}



function deleteExpiredPins() {
    let cutoff = new Date();
    const day  = cutoff.getDate();
    cutoff.setDate(day - 21);

    let range = IDBKeyRange.upperBound(cutoff);
    let tx    = db.transaction('pins', 'readwrite');
    let pins  = tx.objectStore('pins');
    let index = pins.index('timestamp');
 
    index.openCursor(range).onsuccess = function(event) {
        let cursor = event.target.result;
        if(!cursor) { return; }
        cursor.delete();
        cursor.continue();
    };
}



function showMessage(msg) {
    let elem = document.getElementById('msg');
    if(!elem) { return; }

    elem.innerText = msg;
    elem.classList.remove('hidden');
    window.clearTimeout(msgTimeout);
    msgTimeout = window.setTimeout(() => elem.classList.add('hidden'), 1500);
}



function getPosition() {
    navigator.geolocation.getCurrentPosition(function(pos) {
        pin = {
            name: null,
            timestamp: new Date(pos.timestamp),
            lat:   pos.coords.latitude,
            long:  pos.coords.longitude,
            error: 180
        };

        getPlaces('find');
    });
}



function getPlaces(action) {
    if(!db) { window.setTimeout(() => getPlaces(action), 100); return; }

    let tx     = db.transaction('places', 'readonly');
    let places = tx.objectStore('places');
    let req    = places.getAll();

    req.onsuccess = event => {
        let records = event.target.result;

        switch(action) {
            case 'find':
                findPlace(records, action);
                break;

            case 'render':
                records.sort((a, b) => { return a.name > b.name });
                renderTable(records, 'places');
                break;

            default: return records;
        }
    };
}



function findPlace(places, action) {
    let place, error, closest;
    let min = 180;

    for(let i in places) {
        place = places[i];
        error = Math.abs(place.lat - pin.lat) + Math.abs(place.long - pin.long);

        if(error < min) {
            min = error;
            closest = place;
        }
    }

    pin.error = min;
    showPinData();

    const name = min < 0.005 ? closest.name : null;
    if(action == 'find') { setPlaceName(name); }
    pin.name = name;

    const now = new Date();
    showMessage('Refreshed ' + formatTimestamp(now, false));
}



function showResult(action=null) {
    let msg = 'No nearby places';

    if(action) {
        msg = 'Checked ' + (action == 'in' ? 'In to ' : 'Out of ');
        if(pin.name) { msg = msg + pin.name; }
    }

    showMessage(msg);
}



function getPlaceName() {
    let input = document.getElementById('name');
    return input ? input.value : null;
}



function setPlaceName(name) {
    let input = document.getElementById('name');
    input.value = name;
}



function showPinData() {
    let time  = document.getElementById('time');
    let lat   = document.getElementById('lat');
    let long  = document.getElementById('long');
    let error = document.getElementById('error');

    if(time  && pin.timestamp) {  time.innerText = formatTimestamp(pin.timestamp); }
    if(lat   && pin.lat)       {   lat.innerText = pin.lat.toFixed(5);   }
    if(long  && pin.long)      {  long.innerText = pin.long.toFixed(5);  }
    if(error && pin.error)     { error.innerText = pin.error.toFixed(5); }
}



function dropPin(type) {
    const name = getPlaceName();

    if(name) {
        if(name != pin.name) { addPlace(name); }
        pin.name = name;
    }

    pin.type = type;
    savePin();
    showResult(type);
}



function addPlace(name) {
    const place = { name, lat: pin.lat, long: pin.long };

    let tx = db.transaction('places', 'readwrite');
    let places = tx.objectStore('places');
    places.add(place);
}



function savePin() {
    let tx = db.transaction('pins', 'readwrite');
    let pins = tx.objectStore('pins');
    pins.add(pin);
    deleteExpiredPins();
}



function getLog() {
    if(!db) { window.setTimeout(() => getLog(), 100); return; }

    let tx   = db.transaction('pins', 'readonly');
    let pins = tx.objectStore('pins');
    let req  = pins.getAll();

    req.onsuccess = event =>  {
        const records = event.target.result;
        renderTable(records, 'log');
    };
}



function renderTable(records, type) {
    let table = document.getElementById('table');
    let div   = document.createElement('div');
    let data, row;

    if(type == 'log') { records.reverse(); }

    for(let i in records) {
        data = records[i];
        row  = type == 'log' ? getLogRow(data) : getPlaceRow(data);
        div.appendChild(row);
    }

    table.innerHTML = div.innerHTML;
}



function getLogRow(pin) {
    let row = document.createElement('TR');
    row.appendChild(cell(pin.id));
    row.appendChild(cell(pin.type));

    const timestamp = formatTimestamp(pin.timestamp);
    row.appendChild(cell(timestamp));

    const coords = pin.lat.toFixed(5) + ', ' + pin.long.toFixed(5);
    let place = pin.name ? pin.name : coords;
    row.appendChild(cell(place));
    return row;
}



function getPlaceRow(place) {
    let row = document.createElement('TR');
    row.appendChild(cell(place.id));
    row.appendChild(cell(place.name));
    return row;
}



function formatTimestamp(t, full=true) {
    const month   = t.getMonth() + 1;
    const day     = t.getDate();
    const minutes = t.getMinutes();
    let hours     = t.getHours();
    let period;

    if(hours > 12) {
        hours  = hours - 12;
        period = 'pm';

    } else {
        period = hours == 12 ? 'pm' : 'am';
        if(hours == 0) { hours = 12; }
    }

    const date = pad(month) + '/' + pad(day);
    const time = pad(hours) + ':' + pad(minutes) + period;
    return (full ? date + ' ' : '') + time;
}



function pad(n) {
    return (n < 10 ? '0': '') + n;
}



function cell(value) {
    let cell = document.createElement('TD');
    let text = document.createTextNode(value ? value : '');
    cell.appendChild(text);
    return cell;
}



function edit(action) {
    let elemPin   = document.getElementById('pin-id');
    let elemPlace = document.getElementById('place-id');
    
    const pin   = parseInt(elemPin.value);
    const place = parseInt(elemPlace.value);
    elemPin.value   = null;
    elemPlace.value = null;

    if(pin && !place) {
        getRecord('pins', pin, action);

    } else if(place && !pin) {
        getRecord('places', place, action);
    }
}



function getRecord(store, key, action) {
    let tx  = db.transaction(store, 'readwrite'); 
    let req = tx.objectStore(store).get(key);

    req.onsuccess = event => editRecord(event.target.result, store, key, action);
}



function editRecord(record, store, key, action) {
    if(!record) { return; }

    let tx = db.transaction(store, 'readwrite');
    let req, name;

    if(action == 'delete') {
        req = tx.objectStore(store).delete(key);

    } else {
        elemName = document.getElementById('name');
        name = elemName.value;
        elemName.value = null;

        record.name = name;
        req = tx.objectStore(store).put(record);
    }

    req.onsuccess = event => {
        let msg = action == 'rename' ? 'Renamed' : 'Deleted';
        msg = msg + (store == 'places' ? ' Place ' : ' Pin ');
        msg = msg + key;
        if(action == 'rename') { msg = msg + ' to ' + name; }

        showMessage(msg);
    }
}
